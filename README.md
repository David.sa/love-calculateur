# love-calculateur
## Pésentation du love-calculateur 1.0.0
Le but de ce projet est d'utilisé les competences aquise en **javascript** durant les cours, et d'en developper de nouvelle grace a l'auto-formation.

## Languages utilisées
* **HTML**
* **CSS**
* **JAVASCRIPT**
    * 
    * 

## Interet
1. Mise en pratique des connaissances

2. utilisation et apprentissage du **javascript**

## Modifications

## Exemple de code utilisé


```
function love(){
        var vjour = document.formulaire.vjour.value
        var sjour = document.formulaire.sjour.value
        var vmois = document.formulaire.vmois.value
        var smois = document.formulaire.smois.value
        var sommejours = vjour+sjour
        var sommemois = vmois+smois
        var rad_vjour = ((sommejours)*Math.PI)/180
        var rad_vmois = ((sommemois)*Math.PI)/180
      
       jx = Math.cos(rad_vjour)
       
       mx = Math.sin(rad_vmois) 
      
       sx = Math.abs(jx + mx) 
       expr = 100*sx
       if (expr>100){
           alert("ho la la quelle chance +99%")
           }
       if (expr<100){
           alert("le r�sultat est de R={"+expr+" %} pour ces deux dates"+vjour+"/"+vmois+" et "+sjour+"/"+smois)
           }
       }
```
## Jeux

voila la page d'acceuille qui presente le concept du jeu

![20% center](./img/lovecalcul.png)

## Difficultés

Je n'ai pas eu de difficulté parctiiculière, mais c'etait projet relativement facile.

## Copyright

©2018-2019 DavidSalvador All right reserved.

(pour voir mes autre création rendez-vous sur [Gitlab](https://gitlab.com/dashboard/projects).)

